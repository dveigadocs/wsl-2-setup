# WSL 2 setup guide
This guide will help you setup your WSL2 environment to develop native docker applications.

## Requirements
 - Windows v2004 or newer
 - Windows Subsystem for linux (WLS) version 2

### Get current version of Windows
  1. Click on the start button
  1. Selct the settings wheel
  1. Click on System
  1. Scroll down to About
     - There should be a Systems specification near the bottom with Edition and Version

### Updating to Windows 2004
  1. Run Find Updates
    ![](./screenshots/01-find-updates.png)
  1. Click on *Download and install*.
    ![](./screenshots/02-update-to-2004.png)
     -  You may need manually look for updates in order for this option to appear
     - **Important** If the option does not show up, it may not be compatible with your hardware yet. **I don't recommend** following online tutorials that explain how to force the update

### Setup WSL 2
  1. Open a powershell as **administrator** and run the following command to enable wsl2
     ```
     dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
     ```
  1. Run the following coommnad (also in powershell as **administrator**) to enable Virtual Machine platform
     ```
     dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
     ```
  1. **Restart your computer**
  1. Update WSL kernel
     - Use [this link](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) to download the kernel updater that enables WSL 2 functions
     - Otherwhise the next step will throw an error
  1. Set WSL 2 as default
     ```
     wsl --set-default-version 2
     ```
     ![](./screenshots/03-set-wsl-version.png)
  1. You can now use the windows store to install several linux distros
      - [Ubuntu](https://www.microsoft.com/store/productId/9N6SVWS3RX71)
      - [Debian](https://www.microsoft.com/store/productId/9MSVKQC78PK6)
  1. You can also install a proper version of docker for windows
      - [Install docker For windows](https://docs.docker.com/docker-for-windows/install/): Installers' page on [hub.docker.com](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
      - **Make sure you check the WSL 2 option during installation**
        - ![](./screenshots/04-docker-windows-wsl2.png)
